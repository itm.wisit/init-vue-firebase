import Vue from 'vue'
import Router from 'vue-router'
import MainPage from '@/components/Main'
import ManageUserPage from '@/components/ManageUser'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: MainPage
    },
    {
      path: '/manage-user',
      name: 'ManageUser',
      component: ManageUserPage
    }
  ]
})
